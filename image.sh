#!/bin/bash

dice=(images/event_processing.png)

echo '<!DOCTYPE html>'
echo '<html>'
echo '  <head>'
echo '    <meta charset="utf-8">'
echo '    <title>Example</title>'
echo '  </head>'
echo '  <body>'
echo '    <h1>Diagrams</h1>'
echo "    <a href='$dice'><img src='$dice' alt='image' width="90%" style="vertical-align:middle;margin:50px 0px"></a>"
echo '  </body>'
echo '</html>'